const mongoose = require('mongoose')
const schema = mongoose.Schema

const { testConnection, userConnection } = require('../helpers/connection_multi_mongodb')
const bcrypt = require('bcrypt')

const UserSchema = new schema({
    username: {
        type: 'string',
        lowercase: true,
        unique: true,
        require: true,
    },
    password: {
        type: 'string',
        require: true,
    }
})

// const UserSchema1 = new schema({
//     username: {
//         type: 'string',
//         lowercase: true,
//         unique: true,
//         require: true,
//     },
//     password: {
//         type: 'string',
//         require:true,
//     }
// })

// xử lý trước khi save data
UserSchema.pre('save', async function (next) {
    try {
        const salt = await bcrypt.genSalt(10)
        const hashPassword = await bcrypt.hash(this.password, salt)
        this.password = hashPassword
        next()
    } catch (error) {
        next(error)
    }
})

UserSchema.methods.isCheckPassword = async function (password) {
    try {
        return await bcrypt.compare(password, this.password)
    } catch (error) {
        console.log(error);
    }
}

module.exports =
    testConnection.model('User', UserSchema)
    // user: userConnection.model('User', UserSchema1)
