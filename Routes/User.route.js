const express = require('express');
const route = express.Router();
const User = require('../Models/User.model')

const createError = require('http-errors');
const { userValidate } = require('../helpers/validation')
const { signAccessToken, verifyAccessToken, signRefreshToken, verifyRefreshToken } = require('../helpers//jwt_service')
const { redisDel } = require('../helpers/connection_redis');

route.post('/register', async (req, res, next) => {
    try {

        // validation pro
        const { email, password } = req.body
        const { error } = userValidate(req.body)
        if (error) {
            console.log(error.details[0].message)
            throw createError.BadRequest(error.details[0].message)
        }

        // validation cũ
        // if(!email || !password) {
        //     throw createError.BadRequest('Email or password undefine.')
        // }

        const isExist = await User.findOne({ username: email })
        if (isExist) {
            throw createError.Conflict('Email already exists')
        }

        const user = new User({ username: email, password: password })
        const saveUser = await user.save()
        // const isCreate = await User.create({ 
        //     username: email,
        //     password: password
        // })

        return res.json({
            status: 'okay',
            element: saveUser
        })
    } catch (error) {
        next(error);
    }
})

route.post('/login', async (req, res, next) => {
    try {
        const { error } = userValidate(req.body)
        if (error) {
            throw createError.BadRequest(error.details[0].message)
        }

        const user = await User.findOne({ username: req.body.email })
        if (!user) {
            throw createError.NotFound('User not register')
        }

        const isValid = await user.isCheckPassword(req.body.password)
        if (!isValid) {
            throw createError.Unauthorized()
        }
        const accessToken = await signAccessToken(user._id)
        const refreshToken = await signRefreshToken(user._id)
        res.json({
            accessToken,
            refreshToken
        })

    } catch (error) {
        next(error)
    }
})

route.post('/refresh-token', async (req, res, next) => {
    try {
        const { refreshToken } = req.body
        if (!refreshToken) {
            throw createError.BadRequest()
        }

        const {userId} = await verifyRefreshToken(refreshToken)
        const accessToken = await signAccessToken(userId)
        var refToken = await signRefreshToken(userId)
        res.json({
            accessToken,
            refreshToken: refToken
        })
    } catch (error) {
        next(error)
    }
})

route.delete('/logout', async (req, res, next) => {
    try {
        const {refreshToken} = req.body;
        if(!refreshToken) {
            throw createError.BadRequest();
        }
        const {userId} = await verifyRefreshToken(refreshToken);
        redisDel(userId.toString());
        res.json({
            message: 'Logout success.'
        })

    } catch (error) {
        next(error)
    }
})

route.get('/getlists', verifyAccessToken, (req, res, next) => {
    console.log(req.headers);
    const listUsers = [
        {
            username: 'abc@gmail.com'
        },
        {
            username: 'hjk@gmail.com'
        }
    ]
    res.json(listUsers)
})

module.exports = route;