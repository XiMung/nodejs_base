const express = require('express');
const app = express();
const UserRoute = require('./Routes/User.route');
const logEvent = require('./helpers/logEvent')
const {v4 : uuid} = require('uuid')
const helmet = require('helmet')
//handle errors
const createError = require('http-errors');
//
require('dotenv').config();
// require('./helpers/connection_mongoDB')

app.use(express.json());
app.use(express.urlencoded({ extended: true }))
app.use(helmet())

const PORT = process.env.PORT || 5001;

app.get('/', function (req, res, next) {
    res.send('hieu nguyen')
})

app.use('/user', UserRoute);

//handle errors
app.use((req, res, next) => {
    // next(createError.NotFound('This file does not exist.'))
    res.status(404),
        res.json({
            status: 404,
            message: 'Not found!',
            links: '//localhost:'
        })
})
app.use((err, req, res, next) => {
    logEvent(`idError = ${uuid()}-----${req.url}-----${req.method}-----${err.message}`);
    res.status(err.status || 500),
        res.json({
            status: err.status || 500,
            message: err.message
        })
})

app.listen(PORT, () => {
    console.log(`server runing on ${PORT}`);
});