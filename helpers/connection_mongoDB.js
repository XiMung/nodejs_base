const mongoose = require('mongoose');

const conn = mongoose.createConnection('mongodb://localhost:27017/test', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

conn.on('connected', function () {
    console.log(`mongodb connected::: ${this.name}`);
})

conn.on('disconnected', function () {
    console.log(`mongodb disconnected::: ${this.name}`);
})

conn.on('error', function (err) {
    console.log(`mongodb error::::${JSON.stringify(err)}`);
})

process.on('SIGINT', async () => {
    await conn.close();
    process.exit(0);
})

module.exports = conn