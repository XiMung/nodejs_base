// const redis = require('redis');

// const client = redis.createClient({
//     port: 6379,
//     host: '127.0.0.1'
// })

// client.connect((err) => {
//     if(err) {
//         console.log(`err ping` + err);
//     }
// });
// client.on('connect', (err) => console.log('Redis Client connect'));
// client.on('error', (err) => console.log('Redis Client Error', err));
// client.on('ready', (err) => console.log('Redis Client ready'));
// client.ping((err, PONG) => {
//     if(err) {
//         console.log(`err ping` + err);
//     }
//     console.log(PONG);
// })


// module.exports = client;



const { createClient } = require('redis');

const redisSet = async (key, value, EX, valueEX) => {
    const client = createClient();
    client.on('error', (err) => console.log('Redis Client Error', err));
    await client.connect();
    await client.set(`${key}`, `${value}`, { EX : valueEX, NX: true});
    return 'set success';
};

const redisGet = async (key) => {
    const client = createClient();
    client.on('error', (err) => console.log('Redis Client Error', err));
    await client.connect();
    const value = await client.get(`${key}`);
    // console.log(value);
    return value;
};

const redisDel = async (key) => {
    const client = createClient();
    client.on('error', (err) => console.log('Redis Client Error', err));
    await client.connect();
    const value = await client.del(`${key}`);
    return 'delete success';
};

module.exports = {
    redisSet,
    redisGet,
    redisDel
}