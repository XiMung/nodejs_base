const jwt = require('jsonwebtoken')
const createError = require('http-errors');
//using redis
const { redisGet, redisSet } = require('../helpers/connection_redis');

const signAccessToken = async function (userId) {
    return new Promise(function (resolve, reject) {
        const payload = {
            userId: userId
        }
        const secret = process.env.ACCESS_TOKEN_SECRET
        const options = {
            expiresIn: '10h' // 10m 10s
        }

        jwt.sign(payload, secret, options, (err, token) => {
            if (err) {
                reject(err)
            }
            resolve(token)
        })
    })
}

const verifyAccessToken = async function (req, res, next) {
    if (!req.headers['authorization']) {
        return next(createError.Unauthorized())
    }

    const authHeader = req.headers['authorization']
    const bearerToken = authHeader.split(' ')
    const token = bearerToken[1];
    // start verify token
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, payload) => {
        if (err) {
            if (err.name === 'jsonWebTokenError') {
                return next(createError.Unauthorized())
            }
            return next(createError.Unauthorized(err.message))
        }

        req.payload = payload
        next()
    })
}

const signRefreshToken = async function (userId) {
    return new Promise(function (resolve, reject) {
        const payload = {
            userId: userId
        }
        const secret = process.env.REFRESH_TOKEN_SECRET
        const options = {
            expiresIn: '1y' // 10m 10s
        }

        jwt.sign(payload, secret, options, async (err, token) => {
            if (err) {
                reject(err)
            }
            ////// save redis
            redisSet(userId.toString(), token, "EX", 365 * 24 * 60 * 60)
            resolve(token)
            //////////////////////////////////////////////////////////////
        })
    })
}

const verifyRefreshToken = async function (refreshToken) {
    return new Promise((resolve, reject) => {
        jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, payload) => {
            if (err) {
                reject(err)
            }
            redisGet(payload.userId.toString())
                .then(data => {
                    // console.log(data);
                    // console.log(refreshToken);
                    if (refreshToken === data) {
                        return resolve(payload)
                    }else {
                        return reject(createError.Unauthorized())
                    }
                })
                .catch(err => { return reject(createError.InternalServerError()) })

        })
    })

}

module.exports = { signAccessToken, verifyAccessToken, signRefreshToken, verifyRefreshToken }